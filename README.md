# conjugation-service
Simple rest api for spanish conjugations.

Api examples

Return all conjugations for the verb hacer   
   https://127.0.0.1:8080/conjugations/search?verb=hacer&verb=ser&tiempo=1&tiempo=2
   tiempo is not required.  There is no limit to to the number required.



Return all conjugations for the verb hacer   
   https://127.0.0.1:8080/conjugations/hacer
   
Return all Present tense conjugations for the verb hacer.
	https://127.0.0.1:8080/conjugations/hacer/IndicativoPresente
	
Get a list of all the tense available
	https://127.0.0.1:8080/tiempo
	

 
