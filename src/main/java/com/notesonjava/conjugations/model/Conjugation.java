package com.notesonjava.conjugations.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Conjugation {
	
	
	@NonNull
	private String word;
	
	@NonNull
	private Persona persona;
	


}