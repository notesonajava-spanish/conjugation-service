package com.notesonjava.conjugations.model.dto;

import com.notesonjava.conjugations.model.Tiempo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TiempoDto {
	private int id;
	private Tiempo tiempo;

	public TiempoDto(Tiempo tiempo) {
		this.tiempo = tiempo;
		this.id = tiempo.getValue();
	}
}
