package com.notesonjava.conjugations;


import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.dto.TiempoDto;

import io.javalin.Javalin;
import lombok.AllArgsConstructor;


@AllArgsConstructor
public class TiempoController {

	private static final int HTTP_OK = 200;
	private ObjectMapper mapper;
	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";

	
	public void init(Javalin app) {
		app.get("/tiempos", ctx -> {
			ctx.status(HTTP_OK);
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
	
			List<TiempoDto> result = of(Tiempo.values()).map(t -> new TiempoDto(t)).collect(toList());
			ctx.result(mapper.writeValueAsString(result));
		
		});
	}
	
}
