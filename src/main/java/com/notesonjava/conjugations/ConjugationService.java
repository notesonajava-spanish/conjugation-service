package com.notesonjava.conjugations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationService {


	private ConjugationRepository repo;

	public Collection<VerbConjugation> searchConjugations(List<String> verbs, List<String> tiempos,
			List<String> personas) {
		Collection<VerbConjugation> result = new ArrayList<>();
		if (tiempos != null && !tiempos.isEmpty()) {
			if (personas != null && !personas.isEmpty()) {
				List<Tiempo> tiempoList = tiempos.stream().map(name -> convertTiempo(name)).collect(Collectors.toList());
				List<Persona> personaList = personas.stream().map(name -> convertPersona(name)).collect(Collectors.toList());   
				Collection<VerbConjugation> list = search(verbs, tiempoList, personaList);
				result.addAll(list);
			} else {
				List<Tiempo> tiempoList = tiempos.stream().map(name -> convertTiempo(name)).collect(Collectors.toList());
				result.addAll(search(verbs, tiempoList));
			}
		} else {
			result.addAll(search(verbs, new ArrayList<>()));
		}
		return result;
	}
    
	public Collection<VerbConjugation> search(List<String> verbs, List<Tiempo> tiempos) {
        List<VerbConjugation> list = new ArrayList<>();

        if (verbs != null && !verbs.isEmpty()) {
        	if (tiempos == null ||  tiempos.isEmpty()) {
                list = repo.findByVerbIn(verbs).toList().blockingGet();
            } else {
            	list = repo.findByTiempoInAndVerbIn(tiempos, verbs).toList().blockingGet();
            }
        } else {
        	if (tiempos != null && !tiempos.isEmpty()) {
        		list = repo.findByTiempos(tiempos).toList().blockingGet();
        	}
        }
        return list;
	}

	public Collection<VerbConjugation> search(List<String> verbs, List<Tiempo> tiempos, List<Persona> personas) {
        List<VerbConjugation> list = new ArrayList<>();

        if (verbs != null && !verbs.isEmpty()) {
        	if (tiempos == null ||  tiempos.isEmpty()) {
                list = repo.findByVerbIn(verbs).toList().blockingGet();
            } else {
            	if (personas == null || personas.isEmpty()) {
            		list = repo.findByTiempoInAndVerbIn(tiempos, verbs).toList().blockingGet();
            	} else {
            		list = repo.findByTiempoInAndVerbIn(tiempos, verbs, personas).toList().blockingGet();
            	}
            	
            }
        } else {
        	if (tiempos != null && !tiempos.isEmpty()) {
        		list = repo.findByTiempos(tiempos).toList().blockingGet();
        	}
        }
        return list;
	}
	
    public Collection<VerbConjugation> findByVerb(String verb) {
    	return repo.findByVerb(verb).toList().blockingGet();
    }

    public Collection<VerbConjugation> findByTiempo(Tiempo tiempo) {
        return repo.findByTiempo(tiempo).toList().blockingGet();
    }

    public Collection<VerbConjugation> findByTiempoAndVerbSuffix(String suffix, List<Tiempo> tiempos) {
     	return repo.findByTiempoInAndVerbEndsWith(tiempos, suffix).toList().blockingGet();   
    }

    public Collection<VerbConjugation> findByTiempoAndVerbNotIn(Tiempo tiempo,  List<String> verbs) {
        return repo.findByTiempoAndVerbNotIn(tiempo, verbs).toList().blockingGet();
    }

    private Persona convertPersona(String personaValue) {
		Integer id = Integer.parseInt(personaValue);
		return Persona.valueOf(id);
	}
    private Tiempo convertTiempo(String tiempoValue) {
		Integer id = Integer.parseInt(tiempoValue);
		return Tiempo.valueOf(id);
	}
}
