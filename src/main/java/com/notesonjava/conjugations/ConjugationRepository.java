package com.notesonjava.conjugations;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.not;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.reactivestreams.Publisher;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;

import io.reactivex.Flowable;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ConjugationRepository {

	private static final String VERB = "verb";
	private static final String TIEMPO = "tiempo";
	private static final String CONJUGATIONS = "conjugations";
	@NonNull
	private MongoDatabase database;
	@NonNull
	private DocumentAdapter adapter;

	public Flowable<VerbConjugation> findByTiempo(Tiempo tiempo) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.fields(Projections.excludeId());
		Bson filter = Filters.eq(TIEMPO, tiempo.getValue());
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}
    
	public Flowable<VerbConjugation> findByTiempo(Tiempo tiempo, List<Persona> personas) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.fields(Projections.excludeId());
		Bson filter = Filters.eq(TIEMPO, tiempo.getValue());
		Publisher<Document> publisher =  collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc, personas));

	}

	public Flowable<VerbConjugation> findByTiempos(List<Tiempo> tiempos) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Set<Integer> idList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toSet());
		Bson projection = Projections.fields(Projections.excludeId());
		Bson filter = Filters.in(TIEMPO, idList);
		Publisher<Document> publisher =  collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByTiempos(List<Tiempo> tiempos, List<Persona> personas) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Set<Integer> idList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toSet());
		Bson projection = Projections.fields(Projections.excludeId());
		Bson filter = Filters.in(TIEMPO, idList);
		Publisher<Document> publisher =  collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByVerb(String verb) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson filter = Filters.eq(VERB, verb);
		Publisher<Document> publisher =  collection.find(filter).projection(Projections.excludeId());
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByVerb(String verb, List<Persona> personas) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson filter = Filters.eq(VERB, verb);
		Publisher<Document> publisher =  collection.find(filter).projection(Projections.excludeId());
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc, personas));
	}

	public Flowable<VerbConjugation> findByVerbIn(List<String> verbs) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);

		Bson filter = Filters.in(VERB, verbs);
		Bson projection = Projections.excludeId();
		Publisher<Document> publisher =  collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByVerbIn(List<String> verbs, List<Persona> personas) {
		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);

		Bson filter = Filters.in(VERB, verbs);
		Bson projection = Projections.excludeId();
		Publisher<Document> publisher =  collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc, personas));
	}
    
	public Flowable<VerbConjugation> findByVerbAndTiempo(String verb, Tiempo tiempo) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.eq(VERB, verb), Filters.eq(TIEMPO, tiempo.getValue()));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByVerbAndTiempo(String verb, Tiempo tiempo, List<Persona> personas) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.eq(VERB, verb), Filters.eq(TIEMPO, tiempo.getValue()));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByTiempoAndVerbIn(Tiempo tiempo, List<String> verbs) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.in(VERB, verbs));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByTiempoAndVerbIn(Tiempo tiempo, List<String> verbs, List<Persona> personas) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.in(VERB, verbs));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByTiempoAndVerbNotIn(Tiempo tiempo, List<String> verbs) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);

		Bson projection = Projections.excludeId();

		Bson filter = and(not(in(VERB, verbs)), eq(TIEMPO, tiempo.getValue()));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByTiempoAndVerbNotIn(Tiempo tiempo, List<String> verbs,
			List<Persona> personas) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);

		Bson projection = Projections.excludeId();

		Bson filter = and(not(in(VERB, verbs)), eq(TIEMPO, tiempo.getValue()));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByTiempoInAndVerbIn(List<Tiempo> tiempos, List<String> verbs) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());

		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(in(VERB, verbs), in(TIEMPO, tiempoList));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));

	}

	public Flowable<VerbConjugation> findByTiempoInAndVerbIn(List<Tiempo> tiempos, List<String> verbs,
			List<Persona> personas) {


		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());

		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(in(VERB, verbs), in(TIEMPO, tiempoList));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc, personas));

	}

	public Flowable<VerbConjugation> findByTiempoInAndVerbEndsWith(List<Tiempo> tiempos, String verbSuffix) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.regex(VERB, verbSuffix + "$"), Filters.in(TIEMPO, tiempoList));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}

	public Flowable<VerbConjugation> findByTiempoInAndVerbEndsWith(List<Tiempo> tiempos, String verbSuffix,
			List<Persona> personas) {

		MongoCollection<Document> collection = database.getCollection(CONJUGATIONS);
		List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());
		Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.regex(VERB, verbSuffix + "$"), Filters.in(TIEMPO, tiempoList));
		Publisher<Document> publisher = collection.find(filter).projection(projection);
		return Flowable.fromPublisher(publisher).map(doc -> adapter.convert(doc));
	}
}
