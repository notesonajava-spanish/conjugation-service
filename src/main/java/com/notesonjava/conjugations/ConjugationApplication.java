package com.notesonjava.conjugations;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;

import io.javalin.Javalin;
import io.javalin.core.util.RouteOverviewPlugin;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConjugationApplication {
	
	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
	
	public static MongoClient createLocalClient() {
		return MongoClients.create();
	}
	
	public static MongoClient createLocalClient(String host, int port) {
		return MongoClients.create("mongodb://" + host + ":" + port);
		
	}
	
	
	public static Javalin createApp() {
		Javalin app = Javalin.create(config -> {
			config.requestLogger((ctx, ms) -> {
			    log.info(ctx.method() + " "  + ctx.path() + " took " + ms + " ms");
		    });
			config.registerPlugin(new RouteOverviewPlugin("/"));
			config.defaultContentType = CONTENT_TYPE;			
	
		});
		return app;
	}
	
	
	
    public static void main(String[] args) throws IOException {
     	
    	PropertyMap props = PropertyLoader.loadProperties();
    	String serverPort = props.get("server.port").orElse("8082");
    	String mongoPort = props.get("mongo.port").orElse("27017");
    	String mongoHost = props.get("mongo.host").orElse("localhost");
    	String mongoDb = props.get("mongo.db").orElse("conjugations");
		
    	int port = Integer.parseInt(serverPort);
    	int dbPort = Integer.parseInt(mongoPort);
    	
    	ObjectMapper mapper = new ObjectMapper();
    	MongoClient mongoClient = createLocalClient(mongoHost, dbPort);
    	MongoDatabase database = mongoClient.getDatabase(mongoDb);
		
		DocumentAdapter adapter = new DocumentAdapter();
    	
		ConjugationRepository repo = new ConjugationRepository(database, adapter);
		TiempoController tiempoController = new TiempoController(mapper);
		
		ConjugationService service = new ConjugationService(repo);
		ConjugationController conjugationController = new ConjugationController(service, mapper);
		
		Javalin app = createApp();
		
		
		
		app.get("/healthz", ctx -> ctx.status(200));
		
		tiempoController.init(app);
		conjugationController.init(app);
		app.start(port);
		
    }
}
