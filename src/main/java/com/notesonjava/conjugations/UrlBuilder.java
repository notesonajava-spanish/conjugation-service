package com.notesonjava.conjugations;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UrlBuilder {
	
    private static final String EXCLUDE = "/exclude/";
	private static final String SUFFIX2 = "/suffix/";
	private static final String SEARCH_URL = "/search?";
	private static final String PERSONA = "persona=";
	private static final String VERB = "verb=";
	private static final String STR = "&";
	private static final String TIEMPO = "tiempo=";
	private String baseUrl;


    public String search(String... verbs) {
        String query = buildQueryString(Arrays.asList(verbs), new ArrayList<>());
        return baseUrl + SEARCH_URL + query;
    }

    public String search(Tiempo... tiempos) {
        String query = buildQueryString(new ArrayList<>(), Arrays.asList(tiempos));
        return baseUrl + SEARCH_URL + query;
    }

    public String search(List<String> verbs, List<Tiempo> tiempos) {
        String query = buildQueryString(verbs, tiempos);
        return baseUrl + SEARCH_URL + query;
    }
    
    public String search(List<String> verbs, List<Tiempo> tiempos, List<Persona> personas) {
        String query = buildQueryString(verbs, tiempos, personas);
        return baseUrl + SEARCH_URL + query;
    }

    public String findByVerb(String verb) {
        return baseUrl + "/verb/" + verb;

    }

    public String findByTiempo(Tiempo tiempo) {
        return baseUrl + "/tiempo/" + tiempo.getValue();
    }

    public String findByVerbAndTiempo(String verb, Tiempo tiempo) {
        return baseUrl + SEARCH_URL + VERB + verb + "&tiempo=" + tiempo.getValue();

    }

    public String findByVerbSuffixAndTiempo(String suffix, Tiempo tiempo) {
        return baseUrl + SUFFIX2 + suffix + "?tiempo=" + tiempo.getValue();

    }

    public String findByVerbSuffixAndTiempos(String suffix, Tiempo...tiempos) {
        return baseUrl + SUFFIX2 + suffix + "?" + buildQueryString(tiempos);
    }


    public String findByTiempoAndVerbNotIn(Tiempo tiempo, List<String> verbs) {
        String query = buildQueryString(verbs.toArray(new String[verbs.size()]));
        return baseUrl + EXCLUDE + tiempo.getValue() + "?" + query;
    }


    private String buildQueryString(Tiempo[] tiempos) {
        StringBuilder sb = new StringBuilder();
        Arrays.asList(tiempos).forEach(t -> sb.append(TIEMPO).append(t.getValue()).append(STR));
        return sb.toString();
    }

    private String buildQueryString(String[] verbs) {
        StringBuilder sb = new StringBuilder();
        Arrays.asList(verbs).forEach(v -> sb.append(VERB).append(v).append(STR));
        return sb.toString();
    }

    private String buildQueryString(List<String> verbs, List<Tiempo> tiempos) {
        StringBuilder sb = new StringBuilder();
        verbs.forEach(v -> sb.append(VERB).append(v).append(STR));
        tiempos.forEach(t -> sb.append(TIEMPO).append(t.getValue()).append(STR));
        return sb.toString();
    }
    
    private String buildQueryString(List<String> verbs, List<Tiempo> tiempos, List<Persona> personas) {
        StringBuilder sb = new StringBuilder();
        verbs.forEach(v -> sb.append(VERB).append(v).append(STR));
        tiempos.forEach(t -> sb.append(TIEMPO).append(t.getValue()).append(STR));
        personas.forEach(p -> sb.append(PERSONA).append(p.getValue()).append(STR));
        return sb.toString();
    }
}
