package com.notesonjava.conjugations;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;

import io.javalin.Javalin;
import lombok.AllArgsConstructor;


@AllArgsConstructor
public class ConjugationController {

    private static final int HTTP_NO_CONTENT = 204;

	private static final int HTTP_OK = 200;
	private static final int HTTP_BAD_REQUEST = 400;
	

	private static final String CONTENT_TYPE = "Content-Type";

	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";

	private ConjugationService service;
    
    private ObjectMapper mapper;
	
    
    public void init(Javalin app) {
    	app.get("/search", ctx -> {
    		ctx.header(CONTENT_TYPE, APPLICATION_JSON_UTF8);
	
    		List<String> verbs = ctx.queryParams("verb");
    		List<String> tiempos = ctx.queryParams("tiempo");
    		List<String> personas = ctx.queryParams("persona");
    		
    		Collection<VerbConjugation> result = service.searchConjugations(verbs, tiempos, personas);
    		if (result.isEmpty()) {
    			ctx.status(HTTP_NO_CONTENT);
    		} else {
    			ctx.status(HTTP_OK);
    		}
    		ctx.result(mapper.writeValueAsString(result));
        	
    	});
    	
    	app.get("/verb/:verb", ctx -> {
    		ctx.header(CONTENT_TYPE, APPLICATION_JSON_UTF8);
    		String verb = ctx.pathParam(":verb");
    		
			Collection<VerbConjugation> result = service.findByVerb(verb);
			if (result.isEmpty()) {
				ctx.status(HTTP_NO_CONTENT);
			} else {
				ctx.status(HTTP_OK);
			}
			ctx.result(mapper.writeValueAsString(result));
    	});
    	
    	app.get("/tiempo/:tiempo", ctx ->  { 
    		ctx.status(HTTP_OK);
			ctx.header(CONTENT_TYPE, APPLICATION_JSON_UTF8);
			String param = ctx.pathParam(":tiempo");
			Tiempo current = convertTiempo(param);
			if (current == Tiempo.NONE) {
				ctx.status(HTTP_BAD_REQUEST);
				ctx.result("Invalid tiempo id : " + param);
			} else {
				Collection<VerbConjugation> result = service.findByTiempo(current);
	    		ctx.result(mapper.writeValueAsString(result));
	    	}
		});
    	app.get("/suffix/:suffix", ctx -> {
    		ctx.status(HTTP_OK);
			ctx.header(CONTENT_TYPE, APPLICATION_JSON_UTF8);
			
    		List<Tiempo> tiempos = ctx.queryParams("tiempo").stream().map(name -> convertTiempo(name)).collect(Collectors.toList());
    		ctx.result(mapper.writeValueAsString(service.findByTiempoAndVerbSuffix(ctx.pathParam(":suffix"), tiempos)));
    	});
    	
    	app.get("/exclude/:tiempo", ctx -> {
    		ctx.status(HTTP_OK);
			ctx.header(CONTENT_TYPE, APPLICATION_JSON_UTF8);
			Tiempo current = convertTiempo(ctx.pathParam(":tiempo"));
			List<String> verbs = ctx.queryParams("verb");
			if (verbs.isEmpty()) {
				Collection<VerbConjugation> result = service.findByTiempo(current);
				ctx.result(mapper.writeValueAsString(result));
			} else {
				List<String> verbsToExclude = verbs;
				Collection<VerbConjugation> result = service.findByTiempoAndVerbNotIn(current, verbsToExclude);
				ctx.result(mapper.writeValueAsString(result));
			} 
    	});
    }


    
	private Tiempo convertTiempo(String tiempoValue) {
		Integer id = Integer.parseInt(tiempoValue);
		return Tiempo.valueOf(id);
	}
}