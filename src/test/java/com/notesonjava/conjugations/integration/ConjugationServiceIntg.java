package com.notesonjava.conjugations.integration;

import static java.util.Arrays.asList;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.conjugations.ConjugationRepository;
import com.notesonjava.conjugations.ConjugationService;
import com.notesonjava.conjugations.DocumentAdapter;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
public class ConjugationServiceIntg {

	
	private static final String MIRAR = "mirar";
	private static ConjugationRepository repo;
    private static MongoClient client;
    private static ConjugationService service;
    
    
    
    @BeforeAll
    public static void init() throws InterruptedException, IOException {
    	PropertyMap props = PropertyLoader.loadProperties();
    	String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
    	client = MongoClients.create("mongodb://"+dbHost+":"+dbPort);
    	
    	
    	MongoDatabase database = client.getDatabase("conjugations");
    	DocumentAdapter adapter = new DocumentAdapter();
    	
    	repo = new ConjugationRepository(database, adapter);
    	service = new ConjugationService(repo);
    }
    
    @AfterAll
    public static void clean() {
    	client.close();    	    	
    }	
    
	
	
	@Test
	public void findSingleVerbWithMultipleTiemposAndMultiplePersonas() throws JsonParseException, JsonMappingException, IOException {

		List<Tiempo> tiempos = asList(Tiempo.IndicativoPresente, Tiempo.ImperativoPresente, Tiempo.Gerundio);
		List<Persona> personas = asList(Persona.NONE, Persona.YO, Persona.TU, Persona.EL, Persona.NOSOTROS, Persona.ELLOS);
		
		
		Collection<VerbConjugation> result = service.search(asList(MIRAR), tiempos, personas);
		
		Assertions.assertThat(result).hasSize(3);

		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSizeLatino(vc);
			Assertions.assertThat(vc.getTiempo()).isIn(tiempos);
		});
	}
	
	
	
}
