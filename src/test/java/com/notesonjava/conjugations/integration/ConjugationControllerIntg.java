package com.notesonjava.conjugations.integration;

import static com.notesonjava.conjugations.model.Tiempo.IndicativoPresente;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.conjugations.ConjugationApplication;
import com.notesonjava.conjugations.ConjugationController;
import com.notesonjava.conjugations.ConjugationRepository;
import com.notesonjava.conjugations.ConjugationService;
import com.notesonjava.conjugations.DocumentAdapter;
import com.notesonjava.conjugations.UrlBuilder;
import com.notesonjava.conjugations.model.Conjugation;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;

import io.javalin.Javalin;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.HttpStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
@Tag("web")
public class ConjugationControllerIntg {


	private static final String MIRAR = "mirar";

	private static UrlBuilder urlBuilder;

	private static final int port = 8085;

	private static final ObjectMapper mapper = new ObjectMapper();

	private static ConjugationController controller;
	private static MongoClient client;

	private JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, VerbConjugation.class);

	private static Javalin app;

	

	@BeforeAll
	public static void init() throws IOException, InterruptedException {

		PropertyMap props = PropertyLoader.loadProperties();

		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));

		app = ConjugationApplication.createApp();
		client = MongoClients.create("mongodb://" + dbHost + ":" + dbPort);
		ConjugationRepository repo = new ConjugationRepository(client.getDatabase("conjugations"),
				new DocumentAdapter());
		ConjugationService service = new ConjugationService(repo);
		
		controller = new ConjugationController(service, mapper);
		urlBuilder = new UrlBuilder("http://localhost:" + port);
		controller.init(app);
		app.start(port);
	}

	@AfterClass
	public static void clean() {

		client.close();		
		app.stop();
	}

	@Test
	public void findByVerb() throws JsonParseException, JsonMappingException, IOException {

		String url = urlBuilder.findByVerb(MIRAR);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);

		Assertions.assertThat(result).hasSize(24);
		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSize(vc);
		});
		
	}

	@Test
	public void failedFindByVerb() {
		String url = urlBuilder.findByVerb("har");
		HttpResponse response = HttpRequest.get(url).send();
		assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_NO_CONTENT);
	}

	@Test
	public void findNonExistingVerb() {

		String url = urlBuilder.findByVerbAndTiempo("har", Tiempo.NONE);

		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_NO_CONTENT);

	}

	@Test
	public void findByTiempo() {
		String url = urlBuilder.findByTiempo(Tiempo.IndicativoPresente);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);

	}

	@Test
	public void searchSingleVerb() throws JsonParseException, JsonMappingException, IOException {

		String url = urlBuilder.search(MIRAR);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		// Assertions.assertThat(result).hasSize(1);
		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSize(vc);
		});
	}
	
	@Test
	public void searchMultipleVerbsWithoutTiempo() throws JsonParseException, JsonMappingException, IOException {

		String url = urlBuilder.search(MIRAR, "estar", "ser");
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(72);
		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSize(vc);

		});
	}

	@Test
	public void searchMultipleVerbsWithSingleTiempos() throws JsonParseException, JsonMappingException, IOException {

		String url = urlBuilder.search(asList(MIRAR, "estar", "ser"), asList(IndicativoPresente));
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);

		Assertions.assertThat(result).hasSize(3);
		result.forEach(vc -> {
			Assertions.assertThat(vc.getConjugations()).hasSize(8);

		});
	}

	@Test
	public void multipleVerbsWithMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<Tiempo> tiempos = asList(IndicativoPresente, Tiempo.IndicativoCondicional);
		String url = urlBuilder.search(Arrays.asList(MIRAR, "estar", "ser"), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);

		Assertions.assertThat(result).hasSize(6);
		result.forEach(vc -> {
			Assertions.assertThat(vc.getConjugations()).hasSize(8);
			Assertions.assertThat(vc.getTiempo()).isIn(tiempos);

		});

	}

	@Test
	public void findSingleVerbWithMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<Tiempo> tiempos = asList(Tiempo.IndicativoPresente, Tiempo.ImperativoPresente);
		String url = urlBuilder.search(Arrays.asList(MIRAR), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(2);

		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSize(vc);
			Assertions.assertThat(vc.getTiempo()).isIn(tiempos);
		});
	}
	
	@Test
	public void findSingleVerbWithMultipleTiemposAndMultiplePersonas() throws JsonParseException, JsonMappingException, IOException {

		List<Tiempo> tiempos = asList(Tiempo.IndicativoPresente, Tiempo.ImperativoPresente, Tiempo.Gerundio);
		List<Persona> personas = asList(Persona.NONE, Persona.YO, Persona.TU, Persona.EL, Persona.NOSOTROS, Persona.ELLOS);
		
		String url = urlBuilder.search(asList(MIRAR), tiempos, personas);
		log.info(url);
		
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(3);

		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSizeLatino(vc);
			Assertions.assertThat(vc.getTiempo()).isIn(tiempos);
		});
	}
	
	

	@Test
	public void failedSearchVerbWithMultipleTiempos() {

		List<Tiempo> tiempos = asList(Tiempo.SubjuntivoPresente, Tiempo.ImperativoPresente);
		String url = urlBuilder.search(Arrays.asList("fumer"), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_NO_CONTENT);

	}

	@Test
	public void findBySuffixAndMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {
		String url = urlBuilder.findByVerbSuffixAndTiempos("ar", Tiempo.IndicativoPresente, Tiempo.SubjuntivoPresente);

		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(1734);
		result.forEach(vc -> {
			VerbConjugationSizeValidator.validateSize(vc);
			Assertions.assertThat(vc.getTiempo())
					.isIn(Arrays.asList(Tiempo.IndicativoPresente, Tiempo.SubjuntivoPresente));
			Assertions.assertThat(vc.getVerb()).endsWith("ar");
		});
	}

	@Test
	public void findByTiempoExcludingVerbs() throws JsonParseException, JsonMappingException, IOException {

		String url = urlBuilder.findByTiempoAndVerbNotIn(Tiempo.IndicativoPresente, Arrays.asList("hablar", "amar"));
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(1207);
		result.forEach(vc -> {
			Assertions.assertThat(vc.getConjugations()).hasSize(8);
			Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.IndicativoPresente);

		});
	}
	
	@Test
	public void findWithAccentGerundio() throws JsonParseException, JsonMappingException, IOException {
		String url = urlBuilder.findByTiempo(Tiempo.GerundioCompuesto);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		log.info(response.bodyText());
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(1209);
		result.forEach(vc -> {
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.GerundioCompuesto);		
		});
		
		Optional<VerbConjugation> optionBanar = result.stream().filter(vc -> vc.getVerb().equals("bañar")).findAny();
		Assertions.assertThat(optionBanar.isPresent()).isTrue();
		Assertions.assertThat(optionBanar.get().getConjugations().get(0).getWord()).isEqualTo("habiendo bañado");
	}
	
	@Test
	public void findWithAccentPreterito() throws JsonParseException, JsonMappingException, IOException {
		String url = urlBuilder.findByTiempo(Tiempo.IndicativoPreteritoPerfectoSimple);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(1209);
		result.forEach(vc -> {
			Assertions.assertThat(vc.getConjugations()).hasSize(8);
			Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.IndicativoPreteritoPerfectoSimple);
		});
		
		Optional<VerbConjugation> optionBanar = result.stream().filter(vc -> vc.getVerb().equals("bañar")).findAny();
		Assertions.assertThat(optionBanar.isPresent()).isTrue();
		Assertions.assertThat(optionBanar.get().getConjugations().get(0).getWord()).isEqualTo("bañé");
		Optional<Conjugation> el = optionBanar.get().getConjugations().stream().filter(c -> c.getPersona() == Persona.EL).findFirst();
		Assertions.assertThat(el.isPresent()).isTrue();
		Assertions.assertThat(el.get().getWord()).isEqualTo("bañó");
		
	}
	

}
