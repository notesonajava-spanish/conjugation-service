package com.notesonjava.conjugations.integration;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.conjugations.ConjugationRepository;
import com.notesonjava.conjugations.DocumentAdapter;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.HttpStatus;

@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
public class ConjugationRepositoryIntg {

    private static final String HACER = "hacer";
	private static final String AMAR = "amar";
	private static ConjugationRepository repo;
    private static MongoClient client;
    
    
    private static final List<Persona> LATINO = asList(Persona.NONE, Persona.YO, Persona.TU, Persona.EL, Persona.NOSOTROS, Persona.ELLOS);
    
    
    
    @BeforeAll
    public static void init() throws InterruptedException, IOException {
    	PropertyMap props = PropertyLoader.loadProperties();
    	String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
    	client = MongoClients.create("mongodb://"+dbHost+":"+dbPort);
    	
    	
    	MongoDatabase database = client.getDatabase("conjugations");
    	DocumentAdapter adapter = new DocumentAdapter();
    	
    	repo = new ConjugationRepository(database, adapter);
    }
    
    @AfterAll
    public static void clean() {
    	client.close();    	    	
    }	
    
    @Test
    public void findByTiempo() throws Exception {
		List<VerbConjugation> result = new ArrayList<>();
        repo.findByTiempo(Tiempo.IndicativoPresente).blockingForEach(vc -> result.add(vc));
      
        assertThat(result).hasSize(1209);
        result.forEach(vc -> {
            Assertions.assertThat(vc.getConjugations()).hasSize(8);
            Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.IndicativoPresente);        
        });
    }
    
    @Test
    public void findByTiempoAndPersonas() throws Exception {
        List<VerbConjugation> result = repo.findByTiempo(Tiempo.IndicativoPresente, asList(Persona.YO, Persona.TU))
        			.toList().blockingGet();

        assertThat(result).hasSize(1209);
        result.forEach(vc -> {
            Assertions.assertThat(vc.getConjugations()).hasSize(2);
            Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.IndicativoPresente);
            
        });
    }

    @Test
    public void findByVerb() throws Exception {
    	List<VerbConjugation> result = new ArrayList<>();
        repo.findByVerb(HACER).blockingForEach(vc -> result.add(vc));
        assertThat(result).hasSize(24);
        result.forEach(vc -> validateSize(vc));
    }
    
    @Test
    public void findByVerbAndPersonas() throws Exception {
    	List<VerbConjugation> result = repo.findByVerb(HACER, LATINO).toList().blockingGet();
        assertThat(result).hasSize(24);
        result.forEach(vc -> validateSizeLatinoPersonas(vc));
    }

    

    @Test
    public void findByVerbIn() throws Exception {

    	List<VerbConjugation> result = repo.findByVerbIn(Arrays.asList(HACER, AMAR))
    					.toList().blockingGet();
        assertThat(result).hasSize(48);
        result.forEach(vc -> validateSize(vc));   	
    }
    
    @Test
    public void findByVerbInAndPersonas() throws Exception {

    	List<VerbConjugation> result = repo.findByVerbIn(Arrays.asList(HACER, AMAR), LATINO)
    					.toList().blockingGet();
        assertThat(result).hasSize(48);
        result.forEach(vc -> validateSizeLatinoPersonas(vc));   	
    }

    @Test
    public void findByVerbAndTiempo() throws Exception {
    	List<VerbConjugation> result = repo.findByVerbAndTiempo(AMAR, Tiempo.Gerundio).toList().blockingGet();
        assertThat(result).hasSize(1);
        result.forEach(vc -> {
            Assertions.assertThat(vc.getConjugations()).hasSize(1);
            assertThat(vc.getConjugations().get(0).getWord()).isEqualTo("amando");
            assertThat(vc.getConjugations().get(0).getPersona()).isEqualTo(Persona.NONE);
            assertThat(vc.getTiempo()).isEqualTo(Tiempo.Gerundio);
        });
        
       
    }
    
    @Test
    public void findByVerbAndTiempoAndPersona() throws Exception {
    	List<VerbConjugation> result = repo.findByVerbAndTiempo(AMAR, Tiempo.Gerundio, asList(Persona.NONE)).toList().blockingGet();
        assertThat(result).hasSize(1);
        result.forEach(vc -> {
            Assertions.assertThat(vc.getConjugations()).hasSize(1);
            assertThat(vc.getConjugations().get(0).getWord()).isEqualTo("amando");
            assertThat(vc.getConjugations().get(0).getPersona()).isEqualTo(Persona.NONE);
            assertThat(vc.getTiempo()).isEqualTo(Tiempo.Gerundio);
        });
        
       
    }
    
   

    @Test
    public void findByTiempoAndVerbIn() throws Exception {
    	List<VerbConjugation> result = repo.findByTiempoAndVerbIn(Tiempo.Participio, asList(AMAR, "hablar"))
    				.toList().blockingGet();
        assertThat(result).hasSize(48);

    }

    @Test
    public void findByTiempoInAndVerbIn() throws Exception {
        List<VerbConjugation> result = repo.findByTiempoInAndVerbIn(asList(Tiempo.Participio, Tiempo.Gerundio), asList(AMAR, "hablar"))
        	.toList().blockingGet();
        assertThat(result).hasSize(4);
        result.forEach(vc -> {
        	Assertions.assertThat(vc.getConjugations()).hasSize(1);
        	
        });
    }

        
    @Test
    public void findByTiempoAndVerbSuffix() throws Exception {
    	List<VerbConjugation> result = repo.findByTiempoInAndVerbEndsWith(asList(Tiempo.Participio), "ar")
        	.toList().blockingGet();
        assertThat(result).hasSize(867);
        result.forEach(vc -> {
        	assertThat(vc.getVerb()).endsWith("ar");
        	assertThat(vc.getConjugations()).isNotEmpty();
        	assertThat(vc.getTiempo()).isEqualTo(Tiempo.Participio);
        	
        });
    }
    
	
    
    private void validateSize(VerbConjugation vc) {
		switch(vc.getTiempo()) {
		case Participio :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Gerundio :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Infinitivo :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case InfinitivoCompuesto :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case ImperativoPresente :
			Assertions.assertThat(vc.getConjugations()).hasSize(7);
			break;			
		case GerundioCompuesto :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;			

		default :
			Assertions.assertThat(vc.getConjugations()).hasSize(8);
			break;
		}
	}
    
    private void validateSizeLatinoPersonas(VerbConjugation vc) {
		switch(vc.getTiempo()) {
		case Participio :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Gerundio :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Infinitivo :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case InfinitivoCompuesto :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case ImperativoPresente :
			Assertions.assertThat(vc.getConjugations()).hasSize(4);
			break;			
		case GerundioCompuesto :
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;			

		default :
			Assertions.assertThat(vc.getConjugations()).hasSize(5);
			break;

		}
	}

   
}