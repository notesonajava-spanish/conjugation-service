package com.notesonjava.conjugations.integration;

import org.assertj.core.api.Assertions;

import com.notesonjava.conjugations.model.VerbConjugation;

public class VerbConjugationSizeValidator {

	public static void validateSize(VerbConjugation vc) {
		switch (vc.getTiempo()) {
		case Participio:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Gerundio:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Infinitivo:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case InfinitivoCompuesto:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case ImperativoPresente:
			Assertions.assertThat(vc.getConjugations()).hasSize(7);
			break;
		case GerundioCompuesto:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		default:
			Assertions.assertThat(vc.getConjugations()).hasSize(8);
			break;
		}
	}
	
	public static void validateSizeLatino(VerbConjugation vc) {
		switch (vc.getTiempo()) {
		case Participio:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Gerundio:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case Infinitivo:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case InfinitivoCompuesto:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		case ImperativoPresente:
			Assertions.assertThat(vc.getConjugations()).hasSize(4);
			break;
		case GerundioCompuesto:
			Assertions.assertThat(vc.getConjugations()).hasSize(1);
			break;
		default:
			Assertions.assertThat(vc.getConjugations()).hasSize(5);
			break;

		}
	}

}
