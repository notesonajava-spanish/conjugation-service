package com.notesonjava.conjugations;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.model.dto.TiempoDto;

import io.javalin.Javalin;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.HttpStatus;

@Tag("Test")
public class TiempoControllerTest {

	private static ObjectMapper mapper = new ObjectMapper();
	private static JavaType type = mapper.getTypeFactory().constructCollectionLikeType(List.class, TiempoDto.class);
	private static Javalin app;
	@BeforeAll
	public static void init(){
		TiempoController controller = new TiempoController(mapper);
		app = Javalin.create();
		controller.init(app);
		app.start(8085);
	}
	
	@AfterAll
	public static void clean() {
		app.stop();
	}
	
    
    @Test
	public void test_list_tiempos() throws JsonParseException, JsonMappingException, IOException{
    	
        HttpResponse response = HttpRequest.get("http://localhost:8085/tiempos").send();
        assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
        
        List<TiempoDto> tiempos = mapper.readValue(response.bodyText(), type);
	    assertThat(tiempos).hasSize(25);
	}
}
