package com.notesonjava.conjugations;

import static com.notesonjava.conjugations.model.Tiempo.IndicativoPresente;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.BDDMockito.given;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.model.Conjugation;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;

import io.javalin.Javalin;
import io.reactivex.Flowable;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.HttpStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Tag("Test")
public class ConjugationControllerTest {

	private static final String HECHO = "hecho";

	private static final String SER = "ser";

	private static final String ESTAR = "estar";

	private static final String HABLA = "habla";

	private static final String HABLAS = "hablas";

	private static final String HABLO = "hablo";

	private static final String HACER = "hacer";

	private static final String HABLAR = "hablar";

	private static final int port = 8085;

	private UrlBuilder urlBuilder = new UrlBuilder("http://localhost:" + port);

	
	private ObjectMapper mapper = new ObjectMapper();

	private ConjugationRepository repo;
	
	
	private JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, VerbConjugation.class);

	private Javalin app;

	@BeforeEach
	public void before() {
		repo = Mockito.mock(ConjugationRepository.class);
		ConjugationService service = new ConjugationService(repo);
		
		ConjugationController controller = new ConjugationController(service, mapper);
		app = Javalin.create();
		controller.init(app);
		app.start(port);
	}
	
	
	@AfterEach
	public void after(){
		app.stop();
	}

	@Test
	public void findByVerb() throws JsonParseException, JsonMappingException, IOException {
		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HACER);
		vc.setTiempo(Tiempo.Participio);
		vc.addConjugation(new Conjugation(HECHO, Persona.NONE));
		list.add(vc);

		given(repo.findByVerb(anyString())).willReturn(Flowable.fromIterable(list));

		String url = urlBuilder.findByVerb(HACER);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		VerbConjugation[] result = mapper.readValue(response.bodyText(), VerbConjugation[].class);
		Assertions.assertThat(Arrays.asList(result)).hasSize(1);

	}

	@Test
	public void failedFindByVerb() {
		given(repo.findByVerb(anyString())).willReturn(Flowable.empty());

		String url = urlBuilder.findByVerb("har");
		HttpResponse response = HttpRequest.get(url).send();
		
		assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_NO_CONTENT);
	}

	@Test
	public void findByInvalidTiempo() {
		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HACER);
		vc.setTiempo(Tiempo.Participio);
		vc.addConjugation(new Conjugation(HECHO, Persona.NONE));
		list.add(vc);

		given(repo.findByVerb(anyString())).willReturn(Flowable.fromIterable(list));

		
		String url = urlBuilder.getBaseUrl()+"/tiempo/45";
		HttpResponse response = HttpRequest.get(url).send();
		
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_BAD_REQUEST);
		Assertions.assertThat(response.bodyText()).contains("Invalid tiempo id : 45");
		
	}
	
	@Test
	public void findByTiempo() {
		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HACER);
		vc.setTiempo(Tiempo.Participio);
		vc.addConjugation(new Conjugation(HECHO, Persona.NONE));
		list.add(vc);
		
		given(repo.findByTiempo(Tiempo.valueOf(1))).willReturn(Flowable.fromIterable(list));
		String url = urlBuilder.findByTiempo(Tiempo.IndicativoPresente);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		
	}

	@Test
	public void searchSingleVerb() throws JsonParseException, JsonMappingException, IOException {
		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);
		
		given(repo.findByVerbIn(anyList())).willReturn(Flowable.fromIterable(list));

		String url = urlBuilder.search(HABLAR);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		
		List<Conjugation> result = mapper.readValue(response.bodyText(), type);
		Assertions.assertThat(result).hasSize(1);
	}

	@Test
	public void searchMultipleVerbsWithoutTiempo() throws JsonParseException, JsonMappingException, IOException {
		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);
		given(repo.findByVerbIn(anyList())).willReturn(Flowable.fromIterable(list));

		String url = urlBuilder.search(ESTAR, SER);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);
	}

	@Test
	public void searchMultipleVerbsWithSingleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);
		
		given(repo.findByTiempoInAndVerbIn(anyList(), anyList())).willReturn(Flowable.fromIterable(list));

		String url = urlBuilder.search(asList(ESTAR, SER), asList(IndicativoPresente));
		log.info(url);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);

		Assertions.assertThat(result).hasSize(1);
		result.forEach(item -> {
			Assertions.assertThat(item.getTiempo()).isEqualTo(IndicativoPresente);
		});
	}

	@Test
	public void multipleVerbsWithMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO,  Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);

		given(repo.findByTiempoInAndVerbIn(anyList(), anyList())).willReturn(Flowable.fromIterable(list));

		List<Tiempo> tiempos = asList(IndicativoPresente, Tiempo.IndicativoCondicional);
		String url = urlBuilder.search(Arrays.asList(ESTAR, SER), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<VerbConjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);
		result.forEach(item -> {
			Assertions.assertThat(vc.getTiempo()).isIn(tiempos);
			
		});
	}

	@Test
	public void findSingleVerbWithMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);

		given(repo.findByTiempoInAndVerbIn(anyList(), anyList())).willReturn(Flowable.fromIterable(list));

		List<Tiempo> tiempos = asList(Tiempo.SubjuntivoPresente, Tiempo.ImperativoPresente);
		String url = urlBuilder.search(Arrays.asList(ESTAR), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<Conjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);

	}
	
	@Test
	public void findSingleVerbWithMultipleTiemposAndMultiplePersonas() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);

		given(repo.findByTiempoInAndVerbIn(anyList(), anyList(), anyList())).willReturn(Flowable.fromIterable(list));

		List<Tiempo> tiempos = asList(Tiempo.SubjuntivoPresente, Tiempo.ImperativoPresente);
		List<Persona> personas = asList(Persona.YO, Persona.TU, Persona.EL);
		String url = urlBuilder.search(Arrays.asList(ESTAR), tiempos, personas);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<Conjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);

	}
	
	
	@Test
	public void failedSearchVerbWithMultipleTiempos() {

		List<VerbConjugation> list = new ArrayList<>();

		given(repo.findByTiempoInAndVerbIn(anyList(), anyList())).willReturn(Flowable.fromIterable(list));

		List<Tiempo> tiempos = asList(Tiempo.SubjuntivoPresente, Tiempo.ImperativoPresente);
		String url = urlBuilder.search(Arrays.asList(ESTAR), tiempos);
		HttpResponse response = HttpRequest.get(url).send();
		
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_NO_CONTENT);

	}
	
	@Test
	public void findBySuffixAndMultipleTiempos() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);

		given(repo.findByTiempoInAndVerbEndsWith(anyList(), anyString())).willReturn(Flowable.fromIterable(list));
		String url = urlBuilder.findByVerbSuffixAndTiempos("ar", Tiempo.IndicativoPresente, Tiempo.IndicativoFuturo);
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<Conjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);
	}
	
	@Test
	public void findByTiempoExcludingVerbs() throws JsonParseException, JsonMappingException, IOException {

		List<VerbConjugation> list = new ArrayList<>();
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(HABLAR);
		vc.setTiempo(Tiempo.IndicativoPresente);
		vc.addConjugation(new Conjugation(HABLO, Persona.YO));
		vc.addConjugation(new Conjugation(HABLAS, Persona.TU));
		vc.addConjugation(new Conjugation(HABLA, Persona.EL));
		list.add(vc);

		given(repo.findByTiempoAndVerbNotIn(isA(Tiempo.class), anyList())).willReturn(Flowable.fromIterable(list));
		
		String url = urlBuilder.findByTiempoAndVerbNotIn(Tiempo.SubjuntivoPresente, Arrays.asList(HABLAR, "amar"));
		
		HttpResponse response = HttpRequest.get(url).send();
		Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.HTTP_OK);
		List<Conjugation> result = mapper.readValue(response.bodyText(), type);
		
		Assertions.assertThat(result).hasSize(1);
	}

}
