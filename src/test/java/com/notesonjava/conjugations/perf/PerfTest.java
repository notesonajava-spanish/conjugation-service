package com.notesonjava.conjugations.perf;


import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.Arrays;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.UrlBuilder;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

@Disabled
public class PerfTest {

	UrlBuilder builder = new UrlBuilder("http://localhost:8082");

	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void grpcTest() throws JsonParseException, JsonMappingException, IOException {
		
		for(int i=0; i<1000; i++) {
			String url = builder.findByTiempo(Tiempo.IndicativoPresente);
			HttpResponse response1 = HttpRequest.get(url).send();
			VerbConjugation[] result = mapper.readValue(response1.bodyBytes(), VerbConjugation[].class);
			Assertions.assertThat(result.length).isEqualTo(1209);


			String url2 = builder.findByTiempo(Tiempo.ImperativoPresente);
			HttpResponse response2 = HttpRequest.get(url2).send();
			VerbConjugation[] result2 = mapper.readValue(response2.bodyBytes(), VerbConjugation[].class);
			Assertions.assertThat(result2.length).isEqualTo(1209);
			

			String url3 = builder.findByTiempo(Tiempo.Gerundio);
			HttpResponse response3 = HttpRequest.get(url3).send();
			VerbConjugation[] result3 = mapper.readValue(response3.bodyBytes(), VerbConjugation[].class);
			Assertions.assertThat(result3.length).isEqualTo(1209);

		}					
	}
	
	@Test
	public void grpcTestPrint() throws JsonParseException, JsonMappingException, IOException {
		
		for(int i=0; i<200; i++) {
			String url = builder.findByTiempo(Tiempo.IndicativoPresente);
			HttpResponse response1 = HttpRequest.get(url).send();
			VerbConjugation[] result = mapper.readValue(response1.bodyBytes(), VerbConjugation[].class);
			Assertions.assertThat(result.length).isEqualTo(1209);
			Arrays.asList(result).forEach(System.out::println);
		}					
	}
}
