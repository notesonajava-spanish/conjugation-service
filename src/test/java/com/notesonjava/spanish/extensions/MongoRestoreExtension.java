package com.notesonjava.spanish.extensions;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.mongo.MongoToolsCommands;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class MongoRestoreExtension implements BeforeAllCallback{

	@NonNull
	private String archive = "conjugations.gz";
	
	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		log.info("Start loading transaction db");
		PropertyMap props = PropertyLoader.loadProperties();
		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		boolean useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		log.info("Load archive : " + archive);
		log.info("Mongo Host: " + dbHost);
		log.info("Mongo Port: " + dbPort);
		if(useCompose) {
			CommandResult importResult = MongoToolsCommands.restoreArchive(archive, dbHost, dbPort );
			importResult.getOutput().forEach(s -> log.warn(s));
		}
		
		log.info("Transaction db should be loaded");
	}
}
